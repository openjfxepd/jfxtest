/*
 * Copyright (C) 2016 John Neffenger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.status6.jfxtest;

import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB_PRE;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.IntBuffer;
import java.util.Map;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 * A simple JavaFX application to display an animated GIF of Duke waving.
 *
 * @author John Neffenger
 */
public class WavingApp extends Application {

    private static final String TITLE = "Waving Duke";
    private static final String IMAGE = "waving.gif";

    private static final String FORMAT = "png";
    private static final String PATHNAME = "frame.png";

    private static final String WIDTH_KEY = "width";
    private static final String WIDTH_DEFAULT = "800";
    private static final String HEIGHT_KEY = "height";
    private static final String HEIGHT_DEFAULT = "600";

    private static int width = 0;
    private static int height = 0;

    /**
     * Captures a frame buffer and saves it in PNG format as the file
     * <i>frame.png</i>. Call this method to capture the frame buffer in a
     * headless environment while debugging.
     *
     * @param buffer the integer frame buffer to capture
     */
    public static void saveFrame(IntBuffer buffer) {
        try {
            BufferedImage awtImage = new BufferedImage(width, height, TYPE_INT_ARGB_PRE);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    awtImage.setRGB(x, y, buffer.get(y * width + x));
                }
            }
            ImageIO.write(awtImage, FORMAT, new File(PATHNAME));
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    /**
     * Initializes the JavaFX application. Reads in the values of the width and
     * height parameters, if present. This method terminates the JavaFX
     * application if the value of the width or height is not an integer.
     */
    @Override
    public void init() {
        Application.Parameters parameters = getParameters();
        Map<String, String> map = parameters.getNamed();
        String w = map.getOrDefault(WIDTH_KEY, WIDTH_DEFAULT);
        String h = map.getOrDefault(HEIGHT_KEY, HEIGHT_DEFAULT);
        try {
            width = Integer.parseInt(w);
            height = Integer.parseInt(h);
        } catch (NumberFormatException e) {
            System.err.println(e);
            Platform.exit();
        }
    }

    /**
     * Starts the JavaFX application.
     *
     * @param stage the primary stage
     */
    @Override
    public void start(Stage stage) {
        InputStream input = WavingApp.class.getResourceAsStream(IMAGE);
        Image image = new Image(input);
        ImageView view = new ImageView(image);

        Pane pane = new StackPane();
        pane.getChildren().add(view);
        Scene scene = new Scene(pane, width, height);

        stage.setTitle(TITLE);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Launches the JavaFX application.
     *
     * @param args the optional named parameters, <em>width</em> and
     * <em>height</em>, providing the scene width and height in pixels. The
     * defaults values are 800 × 600 px (--width=800 --height=600). Note that
     * the Monocle VNC screen is 1024 × 600 px, while the Monocle Headless
     * screen is 1280 × 800 px.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
