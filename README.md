# Waving Duke

This project is a simple JavaFX application that displays an animated GIF of Duke waving at 8⅓ frames per second.

## Project

The NetBeans project uses a Java platform called “JDK 1.8 for x86egl” that contains the Oracle JDK 8 installation with the OpenJFX x86egl bundle overlay.
It includes the four additional run configurations shown below.
The *Patched* run configurations load the [OpenJFX Patches](https://gitlab.com/openjfxepd/jfxpatch) from the class library in `jfxpatch/dist/jfxpatch.jar`.

**Monocle Headless**  
*Parameters:* `width=1280, height=800`  
*VM Options:* `-Dglass.platform=Monocle -Dmonocle.platform=Headless -Dprism.order=sw -Xint`.

**Monocle Headless Patched**  
*Parameters:* `width=1280, height=800`  
*VM Options:* `-Djava.ext.dirs=../jfxpatch/dist:${platforms.JDK_1.8_for_x86egl.home}/jre/lib/ext -Dglass.platform=Monocle -Dmonocle.platform=Headless -Dprism.order=sw -Xint`.

**Monocle VNC**  
*Parameters:* `width=1024, height=600`  
*VM Options:* `-Dglass.platform=Monocle -Dmonocle.platform=VNC -Dprism.order=sw`.

**Monocle VNC Patched**  
*Parameters:* `width=1024, height=600`  
*VM Options:* `-Djava.ext.dirs=../jfxpatch/dist:${platforms.JDK_1.8_for_x86egl.home}/jre/lib/ext -Dglass.platform=Monocle -Dmonocle.platform=VNC -Dprism.order=sw`.

